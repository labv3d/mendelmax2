
//translate([0,0,-37]) rotate([-90,0,00]) import("ventibeigne2.stl");


$fn=32;

extrudeur=52;

module fan(s=1) {
            translate([s*35/2,0,0]) cylinder(d=10,h=3,center=true);
}

module fanTrous(s=1) {
        translate([s*35/2,0,0]) cylinder(d=3.5,h=60,center=true);
}

module carriage(s=1,delta=0) {
            translate([s*40/2,0,0]) cylinder(d=10,h=3+delta,center=true);
}

module carriageTrous(s=1) {
        translate([s*40/2,0,0]) cylinder(d=3.5,h=60,center=true);
}

echo(50/sqrt(2));
echo(35.35*sqrt(2));

module fanb() {
    //difference() {
    union() {
    difference() {
        union() {
            cylinder(r=20,h=20);
            translate([-20,-10,0]) cube([20,30,20]);
        }
        translate([0,0,10]) cylinder(r=15,h=20);
    }
    hull() {
    translate([35/2,35/2,0]) cylinder(d=3.5+8,h=6);
    translate([35/2,-35/2,0]) cylinder(d=3.5+8,h=6);
    translate([-35/2,-35/2,0]) cylinder(d=3.5+8,h=6);
    }
       }
   //fanbTrous();
   //}
}
module fanbHold() {
    difference() {
    hull() {
    translate([35/2,-35/2,-3]) cylinder(d=3.5+8,h=3);
    translate([35/2,35/2,-3]) cylinder(d=3.5+8,h=3);
    }
    fanbTrous();
    }
}
module fanbVent(k=20,w=20) {
    ep=3;
    hull() {
    translate([-20-5/2,-35/2,-3]) cube([10,35+5/2,ep]);
    translate([-20-5/2-k,-w/2,-3]) cube([1,w,ep]);
    translate([-35/2,-35/2,-3]) cylinder(d=3.5+8,h=3);
    }
    //translate([-35/2,-35/2,0]) cylinder(d=3.5,h=60,center=true);
}

/*
%fanb();
difference() {
    fanbVent(30);
    fanbTrous();
}
*/

module fanbMouth() {
    translate([-20,-10,0]) cube([1,30,20]);
}
module fanbTrous() {
    translate([35/2,35/2,0]) cylinder(d=3.5,h=60,center=true);
    translate([35/2,-35/2,0]) cylinder(d=3.5,h=60,center=true);
    translate([-35/2,-35/2,0]) cylinder(d=3.5,h=60,center=true);
}

//attache();

module attache(delta=2) {
    translate([0,0,-delta/2])
    difference() {
        union() {
        carriage(1,delta);
        carriage(-1,delta);
        color("green") scale([1,8/10,1]) difference() {
            cylinder(r=20+5,h=3+delta,center=true);
            cylinder(r=20-5,h=22,center=true);
            translate([-50,0,-50]) cube([100,100,100]);
        }
        }
        carriageTrous(1);
        carriageTrous(-1);
    }
}

module invpos() {
    rotate([0,0,-90])
    rotate([-120,0,0])
    translate([0,40,11])
    child();
}


module pos() {
        translate([0,-40,-11]) rotate([120,0,0]) rotate([0,0,90])  child();
}

module fanbVentComplet() {
    pos() difference() { fanbVent(k=25,w=20); fanbTrous(); }
    translate([-10,-19.3,-extrudeur]) cube([20,3,5]);
}

module final() {
    difference() {
    translate([0,-30,0]) attache();
        pos() fanb();
    }
    pos()     fanbHold();
    pos() %fanb();
}

//fanb();
//fanbHold();

module duplique() {
    children();
    rotate([0,0,180]) children();
}



module lesComplets() {
    duplique() fanbVentComplet();
}


// extrudeur
*translate([0,0,-extrudeur-3]) { 
    translate([0,0,10]) cylinder(r=10,h=50);
    cylinder(r1=1,r2=10,h=10);
}


*  intersection() {
     translate([-50,-50,-30.01]) cube([100,100,100]);
     lesComplets();
  }
 * hull() {
    intersection() {
        translate([0,0,20-extrudeur]) cylinder(r=40,h=2);
        lesComplets();
    }
    translate([0,0,-extrudeur]) cylinder(r=11,h=1);
  }

//  
  duplique() {
    final();
    //fanbVentComplet();
}


// pour imprimer
//invpos() fanbVentComplet();


//fanb();
//fanbMouth();





//color("blue") translate([0,5,0]) rotate([0,180,180]) translate([-25,0,-4]) import("Fan_Mount_4.STL");

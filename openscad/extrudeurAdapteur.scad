//
// adapteur fan e3d 30x30 vers ventilateur 25x24
//

$fa=4;
$fs=0.1;

ep=3;

tol=0.2;

largeur=23.7+tol*2;
hauteur=25+tol*2;


module exterieur() {
    translate([-15,0,-15]) cube([30,ep,30]);
}

module interieur(skin=0,bouts=0,ep=5) {
    hull() {
    rotate([-25,0,0]) translate([-(largeur+skin)/2,-20-bout,-(hauteur+skin)/2-5]) cube([largeur+skin,ep,hauteur+skin]);
    rotate([-15,0,0]) translate([-(largeur+skin-12)/2,1+bout,-(hauteur+skin)/2]) cube([largeur+skin-12,ep,(hauteur+skin-5)]);
    }
}

module trous() {
    for(i=[-12,12]) for(j=[-12,12]) {
        translate([i,0,j]) rotate([90,0,0]) cylinder(d=3,h=3*ep,center=true);
    }
}

module final() {
  difference() {
    union() {
        interieur(skin=1.5,bout=0);
        exterieur();
    }
    interieur(skin=0,bout=0.2);
    trous();
    translate([-50,ep-0.1,-50]) cube([100,100,100]);
  }
}

rotate([-90,0,0])
final();

  
  
  

use <bearing.scad>



// inverse la direction pour tourner?
flip=false;

//color("green") translate([0,0,-100.3]) rotate([90,0,0]) import("spool_foot_v1.stl");

module gear_side() {

	translate([0,15+3,0]) rotate([90,0,0]) 
	intersection() {
		translate([-50,-50,-0.001]) cube([100,100,100]);
		final_gear(T=15,Tcenter=15,hole=false,tol=0.4);
	}

	translate([0,3,0]) rotate([90,0,0]) cylinder(r=6,h=3);
	rotate([-90,0,0]) m_screw_normal([1.8,1.8,3]);

	// ajoute un cone pour le fit
	translate([0,3+9,0]) rotate([90,0,0]) 
	difference() {
		cylinder(r1=25.5,r2=28,h=9,$fn=64);
		cylinder(r1=25.5,r2=25,h=9);
	}

}



module brasold() {
	hull() {
	rotate([90,0,0]) cylinder(r=10,h=5);
	translate([-15,-5,-90]) cube([30,5,5]);
	}

	hull() {
	rotate([90,0,0]) cylinder(r=10,h=15);
	translate([0,0,-15]) rotate([90,0,0]) cylinder(r=2,h=10);
	}
	hull() {
	translate([0,0,-15]) rotate([90,0,0]) cylinder(r=2,h=10);
	translate([-15,-15,-90]) cube([3,15,5]);
	}
	hull() {
	translate([0,0,-15]) rotate([90,0,0]) cylinder(r=2.5,h=10);
	translate([15-3,-15,-90]) cube([3,15,5]);
	}
	difference() {
		translate([0,-15,-90]) scale([1,0.7,1]) cylinder(r=15,h=5);
		translate([0,-20,-91]) cylinder(r=2.75,h=10,$fn=32); // vis
	}
	translate([-15,-15,-90]) cube([30,15,5]);
	translate([-15,-15,-90]) cube([3,15,5]);
	translate([15-3,-15,-90]) cube([3,15,5]);	
}

module bras(hi=110) {
	hull() {
	rotate([90,0,0]) cylinder(r=10,h=5);
	translate([-15,-5,-hi]) cube([30,5,5]);
	}

	rotate([90,0,0]) cylinder(r=10,h=15);
//	translate([0,0,-15]) rotate([90,0,0]) cylinder(r=2,h=10);


	hull() {
	translate([0,0,-10]) rotate([90,0,0]) cylinder(r=2,h=10);
	translate([-15,-15,-hi]) cube([3,15,5]);
	}
	hull() {
	translate([0,0,-10]) rotate([90,0,0]) cylinder(r=2.5,h=10);
	translate([15-3,-15,-hi]) cube([3,15,5]);
	}

	difference() {
		union() {
			translate([0,-15,-hi]) scale([1,0.7,1]) cylinder(r=15,h=5);	
			translate([-15,-15,-hi]) cube([30,15,5]);
			translate([-15,-15,-hi]) cube([3,15,5]);
			translate([15-3,-15,-hi]) cube([3,15,5]);	
		}
	translate([0,-14,-hi-1]) cylinder(r=3.25,h=10,$fn=32); // vis M5
	}
}


module bras_complet(hi=110) {
	difference() {
		//rotate([90,0,0]) cylinder(r=10,h=15);
		bras();
		translate([0,0.001,0])
		rotate([-90,0,0]) m_screw_inverse([1.8,1.8,3]);
	}
	// les cotes (tolerance=0.4 de chaque cote)
	color("red") {
		translate([-13-0.4,-19,-hi-15]) cube([3,19,15]);
		translate([13-3+0.4,-19,-hi-15]) cube([3,19,15]);
	}
}




//m_screw_normal([1.8,1.8,3]);
//translate([15,0,0]) m_screw_inverse([1.8,1.8,3]);


module m_screw_normal(sc=[3,3,3]) {
 	difference() {
			 scale([flip?-1:1,1,1])
	       scale(sc)  rotate([180,0,0]) import("../util/Screw_0.25.stl" );
		translate([0,0,-41]) cylinder(r=20,h=25);
	}
}

module m_screw_inverse(sc=[3,3,3]) {
        difference() {
			scale([1.1,1.1,1]) {
					scale([flip?-1:1,1,1])
                scale(sc) {
                        rotate([180,0,0]) import("../util/Screw_0.25.stl" );
                       translate([0,0,-0.001]) rotate([0,0,45]) import("../util/Screw_0.25.stl" );
                }
			}
               translate([0,0,2]) cylinder(r=20,h=25);
               translate([0,0,-42]) cylinder(r=20,h=25);
        }
}



//////////////////////////////////////////////////////
//////////////////////////////////////////////////////


module final() {
	rotate([-90,0,0]) {
		//gear_side();
		bras_complet();
	}
}

/*
intersection() {
	final();
	//translate([0,0,0]) cube([100,40,10],center=true);  // bras
	//translate([0,0,-0.5]) cube([100,100,11],center=true);  // gear
}
*/



gear_side();
bras_complet();

//final();


// extrusion
% translate([0,0,-120]) cube([20,100,20],center=true);
//difference() {
//	final();
//	translate([0,0,0]) cube([100,150,100],center=true);
//}



//
// soufflet pour ventilateur
//

$fn=64;

mouth=30;
offset=5; // decentrage de l'extrudeur

module fanMouth(ep=2,rc=4) {
    translate([-20,-10-ep,-ep]) cube([rc,30+2*ep,20+2*ep]);
}
module fanMouthAttach(ep=2,rc=6,tol=0.2) {
		difference() {
		hull() {
			translate([-35/2,-35/2,-ep]) cylinder(d=3.5+8,h=ep-tol);
		    	translate([-20,-10-ep,-ep]) cube([rc,30,ep-tol]);
		}
		translate([-20+rc,-50,-50]) cube([100,100,100]);
}
}

module fanTip(w=20,h=10) {
	translate([-20-30,-w/2,0]) rotate([0,30,0]) translate([0,0,0]) cube([1,w,h]);
}
module fanTipIn(w=20,h=10,up=0) {
	#translate([-20-30,-w/2,up]) rotate([0,30,0]) translate([0.4,0,0]) cube([1,w,h]);
}


module fanTrous() {
    translate([35/2,35/2,0]) cylinder(d=3.5,h=60,center=true);
    translate([35/2,-35/2,0]) cylinder(d=3.5,h=60,center=true);
    translate([-35/2,-35/2,0]) cylinder(d=3.5,h=60,center=true);
}


module fan() {
    union() {
    difference() {
        hull() {
            cylinder(r=20,h=20);
            translate([-20,-10,0]) cube([20,30,20]);
        }
        translate([0,0,10]) cylinder(r=15,h=20);
    }
    hull() {
    translate([35/2,35/2,0]) cylinder(d=3.5+8,h=6+14);
    translate([35/2,-35/2,0]) cylinder(d=3.5+8,h=6+14);
    translate([-35/2,-35/2,0]) cylinder(d=3.5+8,h=6+14);
    }
       }
}

//rotate([0,0*(270-30)+1*90,0]) {


module soufflet (offset=0) {
    %difference() {
        fan();
        fanTrous();
    }

    difference() {
     union() {
      hull() {
        fanMouth(ep=2,rc=6);
        translate([0,offset,0]) fanTip(w=mouth,h=10,up=0);
      }
      fanMouthAttach(ep=2,rc=6);
     }
      hull() {
        fanMouth(ep=0,rc=6.1);
        translate([0,offset,0]) fanTipIn(w=mouth-2,h=10-2,up=1);
      }
      fanTrous();
      fan();
    }

}

// left
//translate([40,0,0]) rotate([0,60,0]) soufflet(offset);
// right
//translate([-40,0,0]) rotate([0,60,180]) soufflet(-offset);

// pour imprimer
translate([5,0,0]) rotate([0,90,0]) soufflet(offset);
translate([-5,0,0]) rotate([0,90,180]) soufflet(-offset);




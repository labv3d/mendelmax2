
$fn=128;




module base(hi=0) {
        d=15;
        translate([0,d/2,0]) {
    	difference() {
		#union() {
//			translate([0,-d,-hi]) scale([1,0.7,1]) cylinder(r=d,h=5);	
			translate([-d,-d,-hi]) cube([d*2,d,4]);
			translate([-d,-d,-hi]) cube([3,d,4]);
			translate([d-3,-d,-hi]) cube([3,d,4]);	
		}
                translate([0,-d/2,-hi-1]) cylinder(r=3.25,h=10,$fn=32); // vis M5
	}

	// les cotes (tolerance=0.4 de chaque cote)
	color("red") {
		translate([-d,-d,-hi-d]) cube([3-0.4,d,15]);
		translate([d-3+0.4,-d,-hi-d]) cube([3-0.4,d,15]);
	}
        % translate([0,0,-hi-10]) cube([20,100,20],center=true);
    }
}

w=52;
dist=110;

difference() {
hull() {
    translate([dist,0,0]) cylinder(r=w/2,h=4);
    translate([10,-15/2,0]) cube([50,15,4]);
}

hull() {
    translate([dist,0,-1]) cylinder(r=w/2-5,h=6);
    #translate([10,-5/2,-1]) cube([50,15-10,6]);
}
}

% translate([100,0,0]) cylinder(r=3,h=4);


//bras_complet();


base();
//bras();






